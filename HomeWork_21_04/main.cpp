#include <iostream>
#include <ctime>
int partition(int *pInt, int low, int high);

using namespace std;


int BinarySearchFirstEntry(int *tab, int first, int last, int number){
    if (number < tab[first] || number > tab[last]) return -1;
    while (first<=last){
        int mid = (first + last) / 2;
        if (tab[mid] == number && tab[first] == number){
            return first;
        }else if(tab[mid] == number && tab[first] != number){
            last = mid;
        }else if(tab[mid] < number){
            first = mid + 1;
        } else if(tab[mid] > number){
            last = mid - 1;
        }
    }return -1;
}
int BinarySearchLastEntry(int *tab, int first, int last, int number){
    if (number < tab[first] || number > tab[last]) return -1;
    while (first<=last){
        int mid = (first + last) / 2;
        if (tab[mid] == number && tab[last] == number){
            return last;
        }else if(tab[mid] == number && tab[first] != number){
            last = last - 1;
            first = mid;
        }else if(tab[mid] < number){
            first = mid + 1;
        } else if(tab[mid] > number){
            last = mid - 1;
        }
    }return -1;
}
bool BinarySearch(int *tab, int first, int last, int number){
    while(first <= last){
        int mid = (first + last) / 2;
        if (tab[mid] == number){
            return true;
        }else if (tab[mid] < number){
            first = mid + 1;
        }else if (tab[mid] > number){
            last = mid - 1;
        }
    }return false;
}
bool BinarySearchMatrix(int number, int **matr, int a, int b){
    if (number < matr[0][0] || matr[a - 1][b - 1] < number){
        return false;
    }
    for (int i = 0; i < b;i++){
        if(number <= matr[i][a - 1]){
            return BinarySearch(matr[i],0,a-1, number);
        }
    }return false;
}

int BinarySearchSquareRoot(int number){
    int first = 0;
    int last = number / 2;
    while (last - first > 1){
        int mid = (first + last) / 2;
        int square = mid * mid;
        if (number == square){
            return mid;
        }else if (square < number){
            first = mid;
        }else if (square > number){
            last = mid;
        }
    }return last;
}

int BinarySearchMedianMatrix(int **a, int n, int m) {
    int min = 0;
    int max = 0;
    int minMax = INT_MAX;
    int maxMin = INT_MIN;
    for (int i = 0; i < m; i++) {
        if (a[i][m - 1] > maxMin) {
            max = i;
            maxMin = a[i][m - 1];
        }
        if (a[i][0] < minMax) {
            min = i;
            minMax = a[i][0];
        }
    }

    int first = a[min][0];
    int last = a[max][m - 1];

    while (last - first > 1) {
        int mid = (last + first) / 2;
        int CountNumbersBefore = 0;
        int CountNumbersAfter = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (a[i][j] < mid){
                    CountNumbersBefore++;
                }
                else if (a[i][j] > mid) {
                    CountNumbersAfter++;
                }
            }
        }
        if (((CountNumbersBefore + CountNumbersAfter) % 2 > 0) && (abs(CountNumbersBefore - CountNumbersAfter) <= 1)) {
            return mid;
        }
        if (CountNumbersAfter == CountNumbersBefore){
            return mid;
        }else if(CountNumbersAfter > CountNumbersBefore){
            first = mid;
        } else if (CountNumbersBefore > CountNumbersAfter){
            last = mid;
        }
    }
}
void PrintArray(int *array, int n){
    for (int i = 0; i < n; i++){
        cout << array[i] << " ";
    }cout<<endl;
}

void fillArray(int *array, int n){
    srand( time(NULL));
    array[n] = {};
    for (int i = 0; i < n; i++){
        array[i] = rand() % 10;
    }
}
void quickSort(int arr[], int low, int high){
    if (low < high){
        int pi = partition(arr, low, high);
        quickSort(arr, low, pi - 1);
        quickSort(arr, pi + 1, high);
    }
}
void swap(int* a, int* b){
    int t = *a;
    *a = *b;
    *b = t;
}

int partition (int arr[], int low, int high){
    int pivot = arr[high];
    int i = (low - 1);
    for (int j = low; j <= high- 1; j++){
        if (arr[j] <= pivot){
            i++;
            swap(&arr[i], &arr[j]);
        }
    }
    swap(&arr[i + 1], &arr[high]);
    return (i + 1);
}

void printMatrix(int **arr, int n, int m){
    for(int i = 0; i < n; i++){
        for(int j = 0; j < m; j++)
            cout  << *(*(arr + i) + j)<<" ";
        cout << "\n";
    }
}
void fillMatrix(int **arr, int n, int m){
    srand(time(NULL));
    for(int i = 0; i < n; i++){
        for(int j = 0; j < m; j++)
            *(*(arr + i) + j) = rand() % 16;
    }
}
void swapMatrix(int *x, int *y){
    int t = 0;
    t = *x;
    *x = *y;
    *y = t;
}
void sortMatrix(int **arr, int n, int m){
    for(int k = 0; k < n; k++){
        for(int l = 0; l < m; l++){
            for(int i = 0; i < n; i++){
                for(int j = 0; j < m; j++){
                    if((j != m - 1) && (arr[i][j] > arr[i][j + 1]))
                        swapMatrix(&arr[i][j],&arr[i][j + 1]);
                    else{
                        if((i != n - 1) && (arr[i][j] > arr[i + 1][0]))
                            swapMatrix(&arr[i][j],&arr[i + 1][0]);
                    }
                }
            }
        }
    }
}
void rowsSort(int **arr,int n){
    for (unsigned i = 0; i < n; i++) {
        for (unsigned j = 0; j < n - 1; j++) {
            for (unsigned k = 0; k < n - j - 1; k++) {
                if (arr[i][k] > arr[i][k + 1]) {
                    int tmp = arr[i][k];
                    arr[i][k] = arr[i][k + 1];
                    arr[i][k + 1] = tmp;
                }
            }
        }
    }
}

int main() {
    cout<<"1st Task"<<endl;
    int mass[10];
    cout<<"Enter the size of the array (For example, you can enter 10)"<<endl;
    int x;
    cin>>x;
    fillArray(mass,x);
    quickSort(mass,0,x);
    PrintArray(mass,x);
    cout<<"Enter the number (FIRST occurrence) you want to find: ";
    int number;
    cin>>number;
    cout<<"Position of first : "<<BinarySearchFirstEntry(mass,0,x-1,number)<<endl;// true=1 else =0
    cout<<"Enter the number (LAST occurrence) you want to find: ";
    cin>>number;
    cout<<"Position of first : "<<BinarySearchLastEntry(mass,0,x-1,number)<<endl;// true=1 else =0


    cout<<"2nd Task"<<endl;
    int n = 0, m = 0, **arr;
    cout << "Type Rows: ";
    cin >> n;
    cout << "\nColumns: ";
    cin >> m;
    arr = new int*[n];
    for(int i = 0; i < n; i++)
        *(arr + i) = new int[m];
    cout << "\nMatrix: "<<endl;
    fillMatrix(arr,n,m);
    sortMatrix(arr,n,m);
    printMatrix(arr,n,m);
    cout<<"Enter the number you want to find: ";
    int number2;
    cin>>number2;
    cout<<"The number: "<< BinarySearchMatrix(number2,arr,n,m)<<endl;// true=1 else =0


    cout<<"3rd task \n"<<"Type number to search square root"<<endl;
    int number3;
    cin>>number3;
    cout << "The answer is  " << BinarySearchSquareRoot(number3) << endl;


    cout<<"4rd Task "<<endl;
    int k = 0, l = 0, **array;
    cout << "Type Rows: ";
    cin >> k;
    cout << "\nColumns: ";
    cin >> l;
    array = new int*[k];
    for(int j = 0; j < k; j++)
        *(array + j) = new int[l];
    cout << "\nMatrix: "<<endl;
    fillMatrix(array,k,l);
    rowsSort(array,n);// матрица осортирована по строкам
    printMatrix(array,k,l);
    cout << "The median of the matrix is " << BinarySearchMedianMatrix(array, k, l);

    return 0;
}

