cmake_minimum_required(VERSION 3.19)
project(DoborQuadTree)

set(CMAKE_CXX_STANDARD 20)

add_executable(DoborQuadTree main.cpp)