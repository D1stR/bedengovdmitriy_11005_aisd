#include <iostream>

using namespace std;

struct Node
{
    int item;
    Node * next;
    Node * previous;
};
struct SortedList{
    Node * head = 0;
    Node * end = 0;

    void add(int value){
        Node * node = new Node;
        node->item = value;
        if(!head && !end){
            node->next = 0;
            node->previous = 0;

            Node * nodeHead = new Node;
            nodeHead->item = 0;
            nodeHead->previous = 0;
            nodeHead->next = node;
            head = nodeHead;

            Node * nodeEnd = new Node;
            nodeEnd->item = 0;
            nodeEnd->previous = 0;
            nodeEnd->next = node;
            end = nodeEnd;
            return;
        }

        if(value >= end->next->item){
            if (value == end->next->item){
                cout<<"Value are exist"<<endl;
                return;
            }
            node->next = 0;
            node->previous = end->next;
            end->next->next = node;
            end->next = node;
            cout<<"Element added"<<endl;
            return;
        }
        if (head->next->item >= value) {

            node->next = head->next;
            node->previous = 0;
            head->next->previous = node;
            head->next = node;
            cout<<"Element added"<<endl;
            return;
        }

        int k = size();
        Node * mainHead = end->next;
        while(value <= mainHead->item) {
            if(value == mainHead->item){
                cout<<"Value are exist"<<endl;
                return;
            }
            if (k <= 0 ) {
                cout<<"Element not found"<<endl;
                return;
            }
            mainHead = mainHead->previous;;


        }

        node->next = mainHead->next;
        mainHead->next->previous = node;
        mainHead->next = node;
        node->previous = mainHead;
    }

    int size() {
        int counter = 0;
        Node * temp = head;
        if(head != 0 && end != 0) {
            while(temp->next != 0) {
                counter++;
                temp = temp->next;
            }
            return counter;
        }
        cout<<"Empty"<<endl;
        return 0;
    }
    int get(int id){
        if(id < 0){
            cout<<"Id cannot be less than 0"<<endl;
            return -1;
        }

        if(size() <= id) {
            cout<<"Size smaller id"<<endl;
            return -1;
        }

        int c = 0;
        Node * temp = head->next;
        while(c != id) {
            temp = temp->next;
            c++;
        }
        return temp->item;

    }

    void remove(int id){
        Node * temp = head->next;
        int k = size();
        if(id < 0){
            cout<<"Id incorrect"<<endl;
            return;
        }

        if(k <= id) {
            cout<<"Id incorrect"<<endl;
            return;
        }

        if (id == 0) {
            head->next->next->previous = 0;
            head->next = head->next->next;
            return;
        }

        if (k - 1 == id) {
            end->next->previous->next = 0;
            end->next = end->next->previous;
            return;
        }

        int c = 0;

        while(c != id) {
            c++;
            temp = temp->next;
        }
        temp->previous->next = temp->next;
        temp->next->previous = temp->previous;

    };


    void addUnique(int value){
        Node * node = new Node;
        node->item = value;
        if(!head && !end) {

            node->next = 0;
            node->previous = 0;

            Node * nodeHead = new Node;
            nodeHead->item = 0;
            nodeHead->previous = 0;
            nodeHead->next = node;
            head = nodeHead;

            Node * nodeEnd = new Node;
            nodeEnd->item = 0;
            nodeEnd->previous = 0;
            nodeEnd->next = node;
            end = nodeEnd;
            return;
        }
        if (!head->next)
            return;
        bool flag = true;
        for (int i = 0; i < size(); ++i)
        {
            if (value == get(i))
                flag = false;
        }
        if (flag){
         if(value >= end->next->item){
            if (value == end->next->item){
                cout<<"Value are exist"<<endl;
                return;
            }

            node->next = 0;
            node->previous = end->next;
            end->next->next = node;
            end->next = node;
            cout<<"Element added"<<endl;
            return;
         }

         int k = size();
         Node * mainHead = end->next;
         while(value <= mainHead->item) {
            if(value == mainHead->item){
                cout<<"Value are exist"<<endl;
                return;
            }
            if (k <= 0 ) {
                cout<<"Element not found"<<endl;
                return;
            }
            mainHead = mainHead->previous;;


         }
         if (head->next->item >= value) {
            node->next = head->next;
            node->previous = 0;
            head->next->previous = node;
            head->next = node;
            cout<<"Element added"<<endl;
            return;
         }

         node->next = mainHead->next;
         mainHead->next->previous = node;
         mainHead->next = node;
         node->previous = mainHead;
        }
    }

    SortedList* Union(SortedList * a, SortedList * b){
        SortedList * list = new SortedList;
        int firstList = a->size();
        int secondList = b->size();

        if((firstList == 0) && (secondList == 0)) {
            cout<<"List is empty"<<endl;
            return list;
        }

        for(int i = 0; i < firstList; ++i){
            list->add(a->get(i));
        }

        for(int j = 0; j < secondList; ++j){
            list->add(b->get(j));
        }

        return list;
    };

    SortedList * Intersect(SortedList * a, SortedList * b){
        SortedList * list = new SortedList;

        int firstList = a->size();
        int secondList = b->size();

        if((firstList == 0) || (secondList == 0)) {
            cout<<"List is empty"<<endl;
            return list;
        }

        if((a->get(firstList - 1) < b->get(0)) ||  (a->get(0) > b->get(secondList - 1))) {
            cout<<"No items are the same in lists"<<endl;
            return list;
        }

        for (int i = 0; i < firstList; ++i){
            for (int j = 0; j < secondList; ++j){
                if (firstList == secondList){
                    list->add(a->get(i));
                }
            }
        }
        return list;
    };


    SortedList * Difference(SortedList * a, SortedList * b) {
        SortedList * list = new SortedList;

        int firstList = a->size();
        int secondList = b->size();

        if((firstList == 0) && (secondList == 0)) {
            cout<<"List is empty"<<endl;
            return list;
        }

        if(firstList == 0){
            cout<<"List A is empty"<<endl;
            return list;
        }else{
            for (int i = 0; i < firstList; ++i){
                bool flag = true;
                for (int j = 0; j < secondList; ++j){
                    if (firstList == secondList){
                        flag = false;
                    }
                }
                if (flag == true) {
                    list->add(a->get(i));
                }
            }
            return list;
        }
    };
};

int main() {

    cout<<"Sorted"<<endl;
    SortedList * list = new SortedList;
    list->add(54);
    list->add(22);
    list->add(44);
    list->add(99);
    cout<<"Size: "<<list->size()<<endl;
    cout<<"List:"<<endl;
    for(int i = 0; i < list->size(); i++){
        cout<<list->get(i)<<endl;
    }
    cout<<"Sorted"<<endl;
    SortedList * list4 = new SortedList;
    list4->add(12);
    list4->add(193);
    list4->add(5);

    cout<<"Size: "<<list4->size()<<endl;
    cout<<"List:"<<endl;
    for(int i = 0; i < list4->size(); i++){
        cout<<list4->get(i)<<endl;
    }
    cout<<"Unique"<<endl;
    SortedList * list2 = new SortedList;
    list2->addUnique(55);
    list2->addUnique(55);
    cout<<"List:"<<endl;
    for(int i = 0; i < list2->size(); i++){
        cout<<list2->get(i)<<endl;
    }
    cout<<"Two lists"<<endl;
    SortedList * list3 = list->Union(list,list4);
    cout<<"Size: "<<list3->size()<<endl;
    cout<<"List:"<<endl;
    for(int i = 0; i < list3->size(); i++) {
        cout<<list3->get(i)<<endl;
    }

    cout<<"Intersect"<<endl;
    SortedList * list5 = list->Intersect(list, list4);
    cout<<"Size: "<<list5->size()<<endl;
    cout<<"List:"<<endl;
    for(int i = 0; i < list5->size(); i++) {
        cout<<list5->get(i)<<endl;
    }
    cout<<"Difference"<<endl;
    cout<<"List:"<<endl;
    SortedList * list6 = list->Difference(list, list4);
    for(int i = 0; i < list6->size(); i++){
        cout<<list6->get(i)<<endl;
    }


    return 0;
}
