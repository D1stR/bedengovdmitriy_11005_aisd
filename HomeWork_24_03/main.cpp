#include <iostream>
using namespace std;

struct MaxHeap
{
    int * array = 0;
    int size = 0;
    int MaxSize = 0;

    MaxHeap(int size) {
        array = new int[size];
        MaxSize = size;
    }
    static int Parent(int i) {
        return (i - 1) / 2;
    }
    static int LeftChild(int i) {
        return 2 * i + 1;
    }
    static int RightChild(int i) {
        return 2 * i + 2;
    }


    void SiftUp(int i){
        while( (i > 0) && (array[i] > array[Parent(i)])){
            int temp = array[i];
            array[i] = array[Parent(i)];
            array[Parent(i)] = temp;
            i = Parent(i);
        }
    }

    void SiftDown(int i){
        int maxIndex = i;
        if (LeftChild(i) < size && array[LeftChild(i)] > array[i]){
            maxIndex = LeftChild(i);
        }
        if (RightChild(i) < size && array[RightChild(i)] > array[maxIndex]){
            maxIndex = RightChild(i);
        }
        if (maxIndex != i) {
            int temp = array[i];
            array[i] = array[maxIndex];
            array[maxIndex] = temp;
            SiftDown(maxIndex);
        }
    }


    int GetMax(){
        return array[0];
    }

    int ExtractMax(){
        int result = array[0];
        array[0] = array[size-1];
        size--;
        SiftDown(0);
        return result;
    }

    void Insert(int p){
        if (size < MaxSize){
            size++;
            array[size] = p;
            SiftUp(size);
        }
        else{
            cout << "Heap is full";
        }
    }
};
int * HeapSort(int * array, int size){
    MaxHeap * heap = new MaxHeap(size);

    for(int i = 0; i < size; i++) {
        heap->Insert(array[i]);
    }

    for(int j = 0; j < size; j++) {
        array[j] = heap->ExtractMax();
    }

    return array;
}


int main() {
    MaxHeap *heap = new MaxHeap(5);

    heap->Insert(5);
    heap->Insert(22);
    heap->Insert(7);
    heap->Insert(1);
    heap->Insert(3);

    cout << heap->ExtractMax() << endl;
    cout << heap->ExtractMax() << endl;
    cout << heap->ExtractMax() << endl;
    cout << heap->ExtractMax() << endl;


    cout << endl << "Heap sort" << endl;
    int * a = new int[5]{13,2,42,5,3};
    cout << "Array without sorting: ";
    for (int i = 0; i < 5; i++){
        cout << a[i] << " ";
    }
    a = HeapSort(a,5);
    cout << endl << "Array after sorting:  ";
    for (int i = 0; i < 5; i++){
        cout << a[i] << " ";
    }
    return 0;
}