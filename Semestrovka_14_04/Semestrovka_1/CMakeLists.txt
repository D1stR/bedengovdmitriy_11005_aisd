cmake_minimum_required(VERSION 3.19)
project(Semestrovka_1)

set(CMAKE_CXX_STANDARD 14)

add_executable(Semestrovka_1 main.cpp)