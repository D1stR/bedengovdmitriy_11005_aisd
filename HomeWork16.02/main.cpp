#include <iostream>
#include <cmath>

using namespace std;

void sum() {
    int n;
    int sum = 0;
    int s = 0;
    cout << "Type n: ";
    cin >> n;
    for (int i = 0; i < n; ++i) {
        cin >> s;
        sum = sum + s;
    }
    cout << sum << endl;
}

void radical() {
    double a, b, c, x1, x2 ;
    cin >> a >> b >> c;
    double rad1 ;
    rad1 = b * b - 4 * a * c;
    if (rad1 == 0) {
        x1 = (-b + 0) / (2 * a);
        cout << "x1 : " << x1 <<endl;
    }
    if (rad1 < 0) {
        cout << "No roots"<<endl;
    }
    if (rad1 > 0) {
        x1 = ((-b + sqrt(rad1)) / (2 * a));
        x2 = ((-b - sqrt(rad1)) / (2 * a));
        cout << "х1 : " << x1 << "х2 : " << x2 <<endl;
    }
}

void log() {
    int p = 0;
    double x;
    int a = 1;
    cin >> x;
    if (x<=0){
        cout<<"No answer";
    }
    while (a < x) {
        a = a * 2;
        p++;
    }
    p--;
    cout << p << endl;
}

int power (int x, unsigned p){
    int func = x;
    for (int i = 1; i < p; ++i) {
        func = func * x ;
    }
    return func;
}
int main() {
    int x = 0;
    unsigned  p = 0;
    cout << "Task 1" << endl;
    sum();
    cout << "Task 2" << endl;
    radical();
    cout << "Task 3" << endl;
    log();
    cout << "Task 4\n"<<"Type number and power" << endl;
    cin >> x >> p;
    cout << power(x , p) << endl;

    return 0;
}