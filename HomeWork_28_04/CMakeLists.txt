cmake_minimum_required(VERSION 3.19)
project(HomeWork_28_04)

set(CMAKE_CXX_STANDARD 14)

add_executable(HomeWork_28_04 main.cpp)