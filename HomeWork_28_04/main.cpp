#include "iostream"
#include "math.h"
#include <set>
#include "vector"
#include "bits/stdc++.h"
using namespace std;

int combination(int n){
    int x = 1;
    for (int i = 1; i <= n; ++i)
        x *= i;
    return x;
}

vector<int> getAllCombinations(int x){
    srand (time(NULL));
    int count = to_string(x).length();
    int arrCount[count];
    set<int> combinations;
    vector<int> result;

    for (int i = 0; i < count; ++i){
        arrCount[i] = x % 10;
        x /= 10;
    }
    for (int i = 0; i <combination(count); ++i){

        vector<int> tempVector;
        for(int k = 0; k < count;k++)
            tempVector.push_back(arrCount[k]);
        int combin = 0;
        for (int j = count - 1; j >= 0 ; j--){
            int random = rand()%tempVector.size();
            combin += tempVector.at(random) * pow(10 , j);
            tempVector.erase(tempVector.begin() + random);
        }
        int setSize = combinations.size();
        combinations.insert(combin);
        if(combinations.size() - setSize == 0){
            i--;
            continue;
        }
        result.push_back(combin);
    }
    return result;
}
bool test(int a, int b, int c){
    vector<int> aCombin = getAllCombinations(a);
    vector<int> bCombin = getAllCombinations(b);
    for (int i = 0; i < aCombin.size(); i++){
        for (int j = 0; j < bCombin.size(); j++){
            int sum = aCombin.at(i) + bCombin.at(j);
            if(sum == c)
                return true;
        }
    }
    return false;
}
int numberOfPaths(int length){
    int x = 0;
    int y = 1;
    int answer = 0;
    for (int i = 0;i<length;i++){
        answer = x + y;
        x = y;
        y = answer;
    }
    return answer;
}
int matrix[3][3];

int numberOfPathsMatrix(int n, int m){

    for (int i = 0; i < n; i++){
        matrix[i][0] = 1;
    }
    for (int i = 0; i < m; i ++){
        matrix[n-1][i] = 1;
    }
    for (int i = n-2; i >= 0; i--){
        for (int j = 1; j < m; j++){
            matrix[i][j] = matrix[i+1][j] + matrix[i][j-1];
        }
    }
    return matrix[0][m-1];
}
int main(){
    cout<<"1st task\nA=12 B=31 C=34"<<endl;
    bool result = test(12,31,34);
    cout<<result<<"\n2nd Task\n"<<numberOfPaths(15)<<"\n3rd Task"<<endl;
    cout<<numberOfPathsMatrix(4,4);
}