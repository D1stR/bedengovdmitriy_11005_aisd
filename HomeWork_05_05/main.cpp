#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

void DispensingCoinsFromTheMachine(int x) {
    int one = 0;
    int five = 0;
    int ten = 0;

    if(x == 0){
        cout<<"Exception"<<endl;
        return;
    }
    while (x >= 10) {
        x-=10;
        ten++;
    }
    while (x >= 5){
        x-=5;
        five++;
    }
    while (x > 0){
        x-=1;
        one++;
    }
    cout<<one<<" "<<five<<" "<<ten<<endl;
}
bool compareByValue(int *a, int *b) {
    return a[1] > b[1];
}

int level(int x, int *array) {

    if (array[x] == 1) {
        array[x] = 0;
        return x;
    } else
        while (array[x] != 1 & x >= 0) {
            --x;
        }
    if (x >= 0) {
        array[x] = 0;
        return x;
    }
    return -1;
}
int *programmerWork(vector<int *> a) {
    sort(a.begin(), a.end(), compareByValue);
    int *taskNumbers = new int[6];
    int *levelUp = new int[6]{1,1,1,1,1,1};

    for (int i = 0; i < 6; i++) {
        int *z = a[i];
        taskNumbers[level(z[2] - 1, levelUp)] = z[0];
    }

    return taskNumbers;
}


int main() {
    cout<<"Task 1"<<endl;
    DispensingCoinsFromTheMachine(77);
    cout<<"Task 2"<<endl;

    vector<int *> v;
    int* t1 = new int[3]{ 1,8,5 };
    int* t2 = new int[3]{ 2,3,6 };
    int* t3 = new int[3]{ 3,7,3 };
    int* t4 = new int[3]{ 4,9,3 };
    int* t5 = new int[3]{ 5,3,4 };
    int* t6 = new int[3]{ 6,1,2 };
    v.push_back(t1);
    v.push_back(t2);
    v.push_back(t3);
    v.push_back(t4);
    v.push_back(t5);
    v.push_back(t6);
    int *answer = programmerWork(v);
    for (int i = 0; i < 6; i++) {
        cout << answer[i] << " ";
    }
    return 0;
}
