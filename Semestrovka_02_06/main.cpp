#include<iostream>
#include<string.h>
#include <math.h>
#include <ctime>
#include <fstream>
#include <sstream>
using namespace std;
void search(char pat[],char text[],int q){
    int lenp = strlen(pat);// lenght of the pattern
    int lent = strlen(text);// lenght of text where pattern is to be checked
    int hashp = 0; //hash value of the pattern
    int hasht = 0; // hash value for checking the text hash value
    int count = 0;// iteration
    for(int i = 0; i<lenp; i++){
        hashp=hashp+int(pat[i])*pow(q,lenp - i - 1);
    }
    for(int i = 0;i < lent-lenp; i++){
        count++;
        for(int j = i;j<i+lenp; j++){
            hasht = hasht+int(text[j])*pow(q,lenp - j - 1 + i);
        }
        if(hashp == hasht){
            cout<<"string found at position "<<i + 1<<endl;
        }
        hasht = 0;
    }
    cout<<"Iteration: "<<count<<endl;
}
string read_string_from_file(const string &file_path) {
    const ifstream input_stream(file_path, ios_base::binary);

    if (input_stream.fail()) {
        throw runtime_error("Failed to open file");
    }

    stringstream buffer;
    buffer << input_stream.rdbuf();

    return buffer.str();
}


int main(){
    char text[20]="abcdabcabcdeac";
    char pat[10]="ab";
    int p = 10;
    cout<<"text here is :"<<text<<endl;
    cout<<"pattern to be checked is :"<<pat<<endl;
    search(pat,text,p);
    char txt[80]="424243242333543535345367567533267868768123987337657";
    char patt[10]="33";
    int f = 10;
    cout<<"text here is :"<<txt<<endl;
    cout<<"pattern to be checked is :"<<patt<<endl;
    search(patt,txt,f);
    string  t3xt = read_string_from_file("D:\\SSD\\CLionProjects\\huyin9\\test.txt");
    //cout<<t3xt;



    double startTime = clock();
    search("59","59594434356464565465464 42  2 2 2 2 2  3 3 3334 54  543 54 456 456 45 23 2 2 ",10);
    double endTime = clock();
    double totalTime = (endTime - startTime) / CLOCKS_PER_SEC;
    cout<<"Duration: "<<totalTime<<endl;

}