#include <iostream>

using namespace std;

void Array(int a[], int size) {
    for (int i = 0; i < size; ++i) {
        cin >> a[i];
    }
}

int sum(int *a, int size) {
    int sum = 0;
    for (int *p = a; p < a + size; ++p) {
        sum = sum + *p;
    }
    return sum;
}
void Arrays(int *array, int size) {
    for (int *l = array; l < array + size;l++) {
        cin >> *l;
    }
}
void sort(int *array, int size) {
    for (int i = size - 1; i > -1; i--) {
        for (int j = 0; j < i; j++) {
            if (array[j] > array[j + 1]) {
                int x = array[j];
                array[j] = array[j + 1];
                array[j + 1] = x;
            }
        }
    }
}

int main() {
    cout << "1st Task"<< endl;
    cout << "Array length = 6"<< endl;
    const int size = 6;
    int a[size];
    cout << "Enter Array numbers "<< endl;
    Array(a, size);
    int *s = &a[0];
    cout << sum(s, size)<<endl;
    cout << "2nd Task"<< endl;
    int *r = new int[8]{1,2,3,4,5,6,7,8};
    int *d = new int[8]{9,10,11,12,13,14,15,16};
    int *aSum = new int[16];
    for(int i = 0; i < 16; i++) {
        if(i < 8) {
            aSum[i] = r[i];
        }
        aSum[i] = d[i - 8];
    }
    delete[] r;
    delete[] d;
    cout<<aSum[3]<<endl<<aSum[15]<<endl;
    cout << "3rd Task"<< endl;
    cout << "Array length"<< endl;
    int n;
    cin >> n;
    int *array = new int[n];
    cout << "Array numbers"<< endl;
    Arrays(array,  n);
    sort(array, n);
    for (int i = 0; i < n; i++) {
        cout << array[i] << endl;
    }
    return 0;
}