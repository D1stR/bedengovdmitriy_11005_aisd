#include <iostream>

using namespace std;

struct Node
{
    int item;
    Node * next;
    Node * previous;
};
struct DoubleLinkedList
{
    Node * head = 0;
    Node * end = 0;

    void add(int item){
        if(!head && !end){
            Node * node = new Node;
            node->item = item;
            node->next = 0;
            node->previous = 0;

            Node * nodeHead = new Node;
            nodeHead->item = 0;
            nodeHead->previous = 0;
            nodeHead->next = node;
            head = nodeHead;

            Node * nodeEnd = new Node;
            nodeEnd->item = 0;
            nodeEnd->previous = 0;
            nodeEnd->next = node;
            end = nodeEnd;

            return;
        }
        Node * node = new Node;
        node->item = item;
        node->next = 0;
        node->previous = end->next;
        end->next->next = node;
        end->next = node;
    }

    int size() {
        if(head != 0 && end != 0) {
            int c = 0;
            Node * temp = head->next;
            while(temp->next != 0) {
                c++;
                temp = temp->next;
            }
            return c+1;
        }
        return 0;
    }

    int get(int id) {

        if(id < 0){
            cout<<"Id cannot be less than 0"<<endl;
            return -1;
        }

        if(size() <= id) {
            cout<<"Size smaller id"<<endl;
            return -1;
        }

        int c = 0;
        Node * temp = head->next;
        while(c != id) {
            temp = temp->next;
            c++;
        }
        return temp->item;
    }

    void insertAt(int item, int id){
        Node * node = new Node;
        node->item = item;
        int k = size();
        if(id != 0){
            cout<<"Id incorrect"<<endl;
            return;
        }

        if(k <= id) {
            cout<<"Id incorrect"<<endl;
            return;
        }

        if (id == 0) {
            node->next = head->next;
            node->previous = 0;
            head->next->previous = node;
            head->next = node;
            return;
        }

        if (k - 1 == id) {
            node->next = 0;
            node->previous = end->next;
            end->next->next = node;
            end->next = node;
            return;
        }

        int c = 0;
        Node * temp = head->next;
        while(c != id) {
            c++;
            temp = temp->next;
        }
        node->next = temp;
        node->previous = temp->previous;
        temp->previous->next = node;
        temp->previous = node;
    }


    void removeAt(int id){
        Node * temp = head->next;
        int k = size();
        if(id < 0){
            cout<<"Id incorrect"<<endl;
            return;
        }

        if(k <= id) {
            cout<<"Id incorrect"<<endl;
            return;
        }

        if (id == 0) {
            head->next->next->previous = 0;
            head->next = head->next->next;
            return;
        }

        if (k - 1 == id) {
            end->next->previous->next = 0;
            end->next = end->next->previous;
            return;
        }

        int c = 0;

        while(c != id) {
            c++;
            temp = temp->next;
        }
        temp->previous->next = temp->next;
        temp->next->previous = temp->previous;
    }
};
struct Queue
{
    Node* head = 0;
    Node* end = 0;

    void  enqueue(int item)
    {
        Node* node = new Node;
        node->item = item;
        node->next = head;
        if (head) {
            head->previous = node;
        }
        head = node;
        if (!end) {
            end = head;
        }
    }

    int dequeue()
    {
        Node* temp = end;
        if (!end)
        {
            cout << "Queue is ended";
            return 0;
        }
        else
        {
            int c = temp->item;
            end = temp->previous;
            delete temp;
            return c;
        }
    }
};
int main()
{

    DoubleLinkedList * list = new DoubleLinkedList;
    Queue * queue = new Queue;
    cout<<"DoubleLinkedList"<<endl;
    list->add(1);
    list->add(3);
    list->add(543);
    list->add(32);

    cout<<list->size()<<endl;//размер 4

    cout<<list->get(0)<<endl;//первый элемент в списке
    cout<<list->get(5)<<endl;//не существует id
    cout<<list->get(-32323)<<endl;//не существует такого id

    list->insertAt(33,0);
    cout<<"Size now: "<<list->size()<<endl;
    cout<<"First element after change: "<<list->get(0)<<endl;

    list->removeAt(4);
    cout<<list->get(4)<<endl;//теперь не существует
    cout<<list->get(3)<<endl;
    cout<<list->get(2)<<endl;
    delete list;
    cout<<endl;

    cout<<"Queue"<<endl;

    queue->enqueue(1);
    queue->enqueue(33);
    queue->enqueue(55);
    queue->enqueue(202);
    queue->enqueue(13);

    cout<<queue->dequeue()<<endl;
    cout<<queue->dequeue()<<endl;
    cout<<queue->dequeue()<<endl;
    cout<<queue->dequeue()<<endl;
    cout<<queue->dequeue();
    delete queue;
    return 0;
}